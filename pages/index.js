import React, { useState, useEffect, memo } from "react";
import { useRouter } from 'next/router'
import {
  Grid,
  CircularProgress,
  Typography,
  Button,
  Tabs,
  Tab,
  TextField,
  Fade
} from "@material-ui/core";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import useStyles from "../src/styles";
import { useUserDispatch, createUser, loginUser, recaptchaSiteverify } from "../components/UserContext";
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const ShowAlert = (open, handleClose, message, severity) => {
  return (
    <>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </>
  )
}
const { IN_SYLVA_reCAPTCHA_site_key } = publicRuntimeConfig

function Login(props) {

  var classes = useStyles();

  // global
  var userDispatch = useUserDispatch();

  // local
  var [isLoading, setIsLoading] = useState(false);
  var [error, setError] = useState(null);
  var [activeTabId, setActiveTabId] = useState(0);
  var [nameValue, setNameValue] = useState("");
  var [loginValue, setLoginValue] = useState("");
  var [passwordValue, setPasswordValue] = useState("");
  var [message, setMessage] = useState("");
  var [open, setOpen] = useState(false);
  var [severity, setSeverity] = useState("info");
  var [grecaptchaValue, setgrecaptchaValue] = useState({});
  const [forgotPassword, setForgotPassword] = useState(false);
  //const [emailValue, setEmailValue] = useState("");
  var [emailValue, setEmailValue] = useState("");
  const router = useRouter();
  const requestType = router.query.requestType;

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  }

  // comment in these lines if you want to disable google recaptcha.
  const onSubmit = async _ => {
    await window.grecaptcha.ready(async () => {
      const result = await window.grecaptcha.execute(`${IN_SYLVA_reCAPTCHA_site_key}`, { action: 'submit' }).then(function async(token) {
        return recaptchaSiteverify(token)
      })
      setgrecaptchaValue(result.content)
    })
  }

  const ForgotPasswordForm = memo(({ emailValue, setEmailValue, classes }) => {

    return (
      <>
        <TextField
          id="emailOLD"
          InputProps={{
            classes: {
              underline: classes.textFieldUnderline,
              input: classes.textField,
            },
          }}
          value={emailValue}
          onChange={e => setEmailValue(e.target.value.toLowerCase())}
          margin="normal"
          placeholder="Your email Adress, please"
          type="email"
          fullWidth />
        <div className={classes.creatingButtonContainer}>
          {isLoading ? (
            <CircularProgress size={26} />
          ) : (
              <Button
                className="g-recaptcha"
                data-sitekey={IN_SYLVA_reCAPTCHA_site_key}
                // data-callback='onSubmit'
                data-action='submit'
                onClick={async () => {
                }}
                disabled={
                  emailValue.length === 0
                }
                size="large"
                variant="contained"
                color="primary"
                fullWidth
                className={classes.createAccountButton}>
                  Validé
              </Button>
            )}
        </div>
      </>
    )
  }, [])

  const onResetPassword = async () => {
    setActiveTabId(2)
  }

  useEffect(() => {
    let isSubscribed = true;
    if (isSubscribed) {
      // comment in these lines if you want to disable google recaptcha.
      const script = document.createElement("script")
      script.src = `https://www.google.com/recaptcha/api.js?render=${IN_SYLVA_reCAPTCHA_site_key}`
      // script.addEventListener('onClick', onSubmit)
      document.body.appendChild(script)
    }
    // cancel subscription to useEffect
    return () => (isSubscribed = false)
  }, [])

  return (
    <>
      <Grid container className={classes.container}>
        <div className={classes.logotypeContainer}>
          <img src="/logo.svg" alt="logo" className={classes.logotypeImage} />
          <Typography className={classes.logotypeText}> IN-SYLVA RESOURCE <br />DISCOVERY PORTAL</Typography>
        </div>
        <div className={classes.formContainer}>
          <div className={classes.form}>
            <Tabs
              value={activeTabId}
              onChange={(e, id) => {
                setActiveTabId(id)
                setError(null)
              }}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto">
              <Tab label="Connexion" /* classes={{ root: classes.tab }} */ />
              <Tab label="Creer Un Compte" /*classes={{ root: classes.tab }} */ />
              <Tab label="Mot de passe" /*classes={{ root: classes.tab }}*/ />
            </Tabs>
            {activeTabId === 0 && (
              <>
                <Typography variant="h1" className={classes.greeting}>
                  Connexion
            </Typography>
                <Fade in={error}>
                  <Typography color="secondary" className={classes.errorMessage}>
                    Something is wrong with your login credentials.
              </Typography>
                </Fade>
                <TextField
                  id="email"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={loginValue}
                  onChange={e => setLoginValue(e.target.value.toLowerCase())}
                  margin="normal"
                  placeholder="Email Adress"
                  type="email"
                  fullWidth
                />
                <TextField
                  id="password"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={passwordValue}
                  onChange={e => setPasswordValue(e.target.value)}
                  margin="normal"
                  placeholder="Password"
                  type="password"
                  fullWidth
                />
                <div className={classes.formButtons}>
                  {isLoading ? (
                    <CircularProgress size={26} className={classes.loginLoader} />
                  ) : (
                      <Button
                        className="g-recaptcha"
                        data-sitekey={IN_SYLVA_reCAPTCHA_site_key}
                        // data-callback='onSubmit'
                        data-action='submit'
                        disabled={
                          loginValue.length === 0 || passwordValue.length === 0
                        }
                        onClick={async () => {
                          // comment in these lines if you want to disable google recaptcha.
                          await onSubmit()
                          if (grecaptchaValue.success) {
                            // Attention 
                            // put out these line from this parentheses if you disable the google recaptcha.
                            await loginUser(router, userDispatch, loginValue, passwordValue, requestType, setIsLoading, setError, setMessage, setOpen)
                          }
                        }}
                        variant="contained"
                        color="primary"
                        size="large"
                      >
                        connectez-vous
                      </Button>
                    )}
                  <Button
                    color="primary"
                    size="large"
                    onClick={onResetPassword}
                    className={classes.forgetButton}>
                    Mot de passe oublie
              </Button>
                </div>
              </>
            )}
            {activeTabId === 1 && (
              <>
                <Typography variant="h1" className={classes.greeting}>
                  Bienvenue!
            </Typography>
                <Typography variant="h2" className={classes.subGreeting}>
                  Creer un nouveau compte.
            </Typography>
                <Fade in={error}>
                  <Typography color="secondary" className={classes.errorMessage}>
                    Something is wrong with your login or password :(
              </Typography>
                </Fade>
                <TextField
                  id="name"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={nameValue}
                  onChange={e => setNameValue(e.target.value.toLowerCase())}
                  margin="normal"
                  placeholder="Username"
                  type="email"
                  fullWidth
                />
                <TextField
                  id="email"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={loginValue}
                  onChange={e => setLoginValue(e.target.value.toLowerCase())}
                  margin="normal"
                  placeholder="Email Adress"
                  type="email"
                  fullWidth
                />
                <TextField
                  id="password"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={passwordValue}
                  onChange={e => setPasswordValue(e.target.value)}
                  margin="normal"
                  placeholder="Password"
                  type="password"
                  fullWidth
                />
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26} />
                  ) : (
                      <Button
                        className="g-recaptcha"
                        data-sitekey={IN_SYLVA_reCAPTCHA_site_key}
                        // data-callback='onSubmit'
                        data-action='submit'
                        onClick={async () => {
                          await onSubmit()

                          if (grecaptchaValue.success) {
                            createUser(
                              userDispatch,
                              nameValue,
                              loginValue,
                              passwordValue,
                              props.history,
                              setIsLoading,
                              setError,
                              setActiveTabId,
                              setMessage,
                              setOpen
                            )
                          }
                        }
                        }
                        disabled={
                          loginValue.length === 0 ||
                          passwordValue.length === 0 ||
                          nameValue.length === 0
                        }
                        size="large"
                        variant="contained"
                        color="primary"
                        fullWidth
                        className={classes.createAccountButton}>
                        Creez votre compte
                      </Button>
                    )}
                </div>
              </>
            )}

            {activeTabId === 2 && (
              <>
                <Typography variant="h1" className={classes.greeting}>
                  Mot de passe perdu !
            </Typography>
                <Typography variant="h2" className={classes.subGreeting}>
                  Saisissez votre adresse courriel.
            </Typography>
                <Fade in={error}>
                  <Typography color="secondary" className={classes.errorMessage}>
                    Something is wrong with your adress :(
              </Typography>
                </Fade>
                <TextField
                  id="email"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={emailValue}
                  onChange={e => setEmailValue(e.target.value.toLowerCase())}
                  margin="normal"
                  placeholder="Your email Adress, please"
                  type="email"
                  fullWidth
                />
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26} />
                  ) : (
                      <Button
                        className="g-recaptcha"
                        data-sitekey={IN_SYLVA_reCAPTCHA_site_key}
                        // data-callback='onSubmit'
                        data-action='submit'
                        onClick={async () => {
                        }}
                        disabled={
                          emailValue.length === 0
                        }
                        size="large"
                        variant="contained"
                        color="primary"
                        fullWidth
                        className={classes.createAccountButton}>
                        Validez
                      </Button>
                    )}
                </div>
              </>
            )}

            {activeTabId === 3 && (
              <>
                <ForgotPasswordForm classes={classes} emailValue={emailValue} setEmailValue={setEmailValue} />
              </>
            )}
            {ShowAlert(open, handleClose, message, severity)}
          </div>
          <Typography color="primary" className={classes.copyright}>
            Developed by <a href="https://www.inrae.fr/" target="_blank">INRAE</a> for <a href="https://www6.inrae.fr/in-sylva-france/Presentation" target="_blank">IN-SYLVA</a> France Research Infrastructure. Licence:&copy;	-BY
      INRAE. {new Date().getFullYear()}
            {'.'}
          </Typography>
        </div>
      </Grid >
    </>
  );
}


export default Login;
