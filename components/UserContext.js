import React from "react"
import KeycloakClient from './KeycloakClient'
import GatekeeperClient from './GatekeeperClient'
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()


var UserStateContext = React.createContext()
var UserDispatchContext = React.createContext()

const kcClient = new KeycloakClient()
const igClient = new GatekeeperClient()

const {
    IN_SYLVA_GATEKEEPER_HOST,
    IN_SYLVA_GATEKEEPER_PORT,

    IN_SYLVA_KEYCLOAK_HOST,
    IN_SYLVA_KEYCLOAK_PORT,

    IN_SYLVA_SEARCH_HOST,
    IN_SYLVA_SEARCH_PORT,

    IN_SYLVA_PORTAL_HOST,
    IN_SYLVA_PORTAL_PORT,

    IN_SYLVA_CLIENT_ID,
    IN_SYLVA_GRANT_TYPE,
    IN_SYLVA_REALM,
    MODE_ENV,
    IN_SYLVA_reCAPTCHA_site_key
} = publicRuntimeConfig

kcClient.baseUrl = MODE_ENV == "development" ? `http://${IN_SYLVA_KEYCLOAK_HOST}:${IN_SYLVA_KEYCLOAK_PORT}/keycloak` : `http://${IN_SYLVA_KEYCLOAK_HOST}`
kcClient.client_id = IN_SYLVA_CLIENT_ID
kcClient.grant_type = IN_SYLVA_GRANT_TYPE
kcClient.realm = IN_SYLVA_REALM

igClient.baseUrl = MODE_ENV == "development" ? `http://${IN_SYLVA_GATEKEEPER_HOST}:${IN_SYLVA_GATEKEEPER_PORT}` : `http://${IN_SYLVA_GATEKEEPER_HOST}`

const searchUrl = MODE_ENV == "development" ? `//${IN_SYLVA_SEARCH_HOST}:${IN_SYLVA_SEARCH_PORT}` : `//${IN_SYLVA_SEARCH_HOST}`
const portalUrl = MODE_ENV == "development" ? `//${IN_SYLVA_PORTAL_HOST}:${IN_SYLVA_PORTAL_PORT}` : `//${IN_SYLVA_PORTAL_HOST}`

function userReducer(state, action) {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return { ...state, isAuthenticated: true }
        case "SIGN_OUT_SUCCESS":
            return { ...state, isAuthenticated: false }
        case "USER_CREATED":
            return { ...state, isAuthenticated: false }
        case "LOGIN_FAILURE":
            return { ...state, isAuthenticated: false }
        case "SERVER_ERROR":
            return { ...state, isAuthenticated: false }
        case "REDIRECT_FAILURE":
            return { ...state, isAuthenticated: false }
        default: {
            throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}

function UserProvider({ children }) {
    var [state, dispatch] = React.useReducer(userReducer, {
        // isAuthenticated: !!localStorage.getItem("id_token"),
    })

    return (
        <UserStateContext.Provider value={state}>
            <UserDispatchContext.Provider value={dispatch}>
                {children}
            </UserDispatchContext.Provider>
        </UserStateContext.Provider>
    )
}

function useUserState() {
    var context = React.useContext(UserStateContext)
    if (context === undefined) {
        throw new Error("useUserState must be used within a UserProvider")
    }
    return context
}

function useUserDispatch() {
    var context = React.useContext(UserDispatchContext)
    if (context === undefined) {
        throw new Error("useUserDispatch must be used within a UserProvider")
    }
    return context
}

export { UserProvider, useUserState, createUser, useUserDispatch, loginUser, signOut, recaptchaSiteverify }

async function loginUser(router, dispatch, login, password, requestType, setIsLoading, setError, setMessage, setOpen) {
    setError(false)
    setIsLoading(true)
    if (!!login && !!password) {
        setTimeout(async () => {
            const result = await kcClient.login({ username: login, password: password })
            if (result && result.access_token && result.refresh_token && requestType) {
                igClient.token = result.access_token
                const { kcId, role } = await igClient.kcId({ email: login })

                if (kcId) {
                    var redirectUrl = ''

                    switch (true) {
                        case (requestType === "search" && (role.role_id === 1 || role.role_id === 2 || role.role_id === 3)):
                            redirectUrl = searchUrl + '/home?kcId=' + kcId + '&accessToken=' + result.access_token + '&refreshToken=' + result.refresh_token + '&roleId=' + role.role_id
                            break
                        case (requestType === "portal" && (role.role_id === 1 || role.role_id === 2)):
                            redirectUrl = portalUrl + '/home?kcId=' + kcId + '&accessToken=' + result.access_token + '&refreshToken=' + result.refresh_token + '&roleId=' + role.role_id
                            break
                        default:
                            if (role.role_id === 3) {
                                dispatch({ type: "REDIRECT_FAILURE" })
                                setError(true)
                                setIsLoading(false)
                                setMessage("Insufficient access rights, you can only access the search application!")
                                setOpen(true)
                            }
                    }
                    dispatch({ type: "LOGIN_SUCCESS" })
                    setError(null)
                    setIsLoading(false)
                    if (!!redirectUrl) {
                        router.push(redirectUrl)
                    }
                } else {
                    dispatch({ type: "SERVER_ERROR" })
                    setIsLoading(false)
                    setMessage("User is not exist at the database!")
                    setOpen(true)
                }
            }
            else {
                dispatch({ type: "REDIRECT_FAILURE" })
                setError(true)
                setIsLoading(false)
            }

        }, 3000)
    } else {
        dispatch({ type: "LOGIN_FAILURE" })
        setError(true)
        setIsLoading(false)
    }
}

async function createUser(dispatch, username, login, password, history, setIsLoading, setError, setActiveTabId, setMessage,
    setOpen) {
    setError(false)
    setIsLoading(true)
    const user = await igClient.createUser({ username: username, email: login, password: password })
    debugger
    if (user.status === 201) {
        dispatch({ type: "USER_CREATED" })
        setError(null)
        setIsLoading(false)
        setActiveTabId(0)
        setMessage("User created successfully.")
        setOpen(true)
    } else {
        if (user.message) {
            dispatch({ type: "LOGIN_FAILURE" })
            setIsLoading(false)
            setMessage(user.message)
            setOpen(true)
        } else {
            dispatch({ type: "LOGIN_FAILURE" })
            setIsLoading(false)
            setMessage("User could not created!.")
            setOpen(true)
        }
    }
}


async function recaptchaSiteverify(token) {
    const status = await igClient.recaptchaSiteverify({ token })
    return status
}

function signOut(dispatch, history) {
    // localStorage.removeItem("id_token")
    dispatch({ type: "SIGN_OUT_SUCCESS" })
    history.push("/login")
}