const fetch = require('node-fetch')

class InSylvaGatekeeperClient {

    async post(method, path, requestContent) {

        const headers = {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + this.token
        }

        const response = await fetch(`${this.baseUrl}${path}`, {
            method: method,
            headers,
            body: JSON.stringify(requestContent),
            mode: 'cors'
        });

        const responseContent = await response.json();

        return responseContent;
    }

    async createUser({
        username,
        email,
        password
    }) {
        const path = `/user`;
        const user = await this.post('POST', `${path}`, {
            username,
            email,
            password,
        });

        return user;
    }

    async kcId({ email }) {
        const path = `/user/kcid`;

        const kcId = await this.post('POST', `${path}`, {
            email: email
        });

        return kcId;
    }

    async recaptchaSiteverify({ token }) {
        const path = `/user/recaptcha/siteverify`
        const result = await this.post('POST', `${path}`, { token })
        return result
    }

}

InSylvaGatekeeperClient.prototype.baseUrl = null;
InSylvaGatekeeperClient.prototype.token = null;
module.exports = InSylvaGatekeeperClient;