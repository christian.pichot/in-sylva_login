const fetch = require('node-fetch')

/*
headers: {
    Authorization: 'Bearer ' + access_token_you_got
}*/
class KeycloakClient {
    async post(path, requestContent) {
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
            // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        };

        var formBody = [];
        for (var property in requestContent) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(requestContent[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        const response = await fetch(`${this.baseUrl}${path}`, {
            method: 'POST',
            headers,
            body: formBody,
            mode: 'cors'
        });

        if (response.ok === true) {
            // ok
        } else {
            throw new Error(response.status);
        }

        const responseContent = await response.json();

        return responseContent;
    };

    async login({
        realm = "in-sylva",
        client_id = this.client_id,
        username,
        password,
        grant_type = this.grant_type
    }) {
        try {
            const path = `/auth/realms/${realm}/protocol/openid-connect/token`;
            const result = await this.post(`${path}`, {
                client_id,
                username,
                password,
                grant_type
            });
            return result
        } catch (err) {
            console.log(err)
            return null
        }
    }

    async refreshToken({
        client_id = this.client_id,
        // client_secret : 'optional depending on the type of client',
        grant_type = "refresh_token",
        refresh_token
    }) {

        console.log("refreshToken function is invoked.")

        const path = `/auth/realms/${realm}/protocol/openid-connect/token`;
        const token = await this.post(`${path}`, {
            client_id,
            grant_type,
            refresh_token
        });

        return { token }

    }
}

KeycloakClient.prototype.baseUrl = null;
KeycloakClient.prototype.client_id = null;
KeycloakClient.prototype.grant_type = null;
KeycloakClient.prototype.realm = null;
module.exports = KeycloakClient;