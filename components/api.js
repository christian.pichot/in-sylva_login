const request = async (method, url, data) => {
    const body = data ? data : {}

    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    const options = {
        method,
        headers,
        // cache: "no-cache",
        // credentials: "same-origin",
        // redirect: "follow",
        // referrerPolicy: "no-referrer",
        body: JSON.stringify(body),
        mode: 'cors'
    }

    return await fetch(url, options)
        .then(async response => {
            const data = await response.json()
            return { status: response.data.status, data }
        })
        .then(result => {
            if (result.status < 200 || result.status >= 3000) throw result

            return result
        })
}

module.exports = {
    get: (url, data) => request("GET", url, data),
    post: (url, data) => request("POST", url, data),
    put: (url, data) => request("PUT", url, data),
    delete: (url, data) => request("DELETE", url, data),
    patch: (url, data) => request("PATCH", url, data)
}