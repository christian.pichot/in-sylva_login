module.exports = {
    pageExtensions: ['mdx', 'jsx', 'js'],
    crossOrigin: 'anonymous',
    preserveLog: true,
    publicRuntimeConfig: {
        MODE_ENV: process.env.MODE_ENV || "development",
        IN_SYLVA_KEYCLOAK_HOST: process.env.IN_SYLVA_KEYCLOAK_HOST || "localhost",
        IN_SYLVA_KEYCLOAK_PORT: process.env.IN_SYLVA_KEYCLOAK_PORT || "7000",
        IN_SYLVA_GATEKEEPER_HOST: process.env.IN_SYLVA_GATEKEEPER_HOST || "localhost",
        IN_SYLVA_GATEKEEPER_PORT: process.env.IN_SYLVA_GATEKEEPER_PORT || "4000",
        IN_SYLVA_SEARCH_HOST: process.env.IN_SYLVA_SEARCH_HOST || "localhost",
        IN_SYLVA_SEARCH_PORT: process.env.IN_SYLVA_SEARCH_PORT || "3001",
        IN_SYLVA_PORTAL_HOST: process.env.IN_SYLVA_PORTAL_HOST || "localhost",
        IN_SYLVA_PORTAL_PORT: process.env.IN_SYLVA_PORTAL_PORT || "3005",
        IN_SYLVA_CLIENT_ID: process.env.IN_SYLVA_CLIENT_ID || "in-sylva.user.app",
        IN_SYLVA_GRANT_TYPE: process.env.IN_SYLVA_GRANT_TYPE || "password",
        IN_SYLVA_REALM: process.env.IN_SYLVA_REALM || "in-sylva",
        IN_SYLVA_reCAPTCHA_site_key: process.env.IN_SYLVA_reCAPTCHA_site_key || "6LflFcoZAAAAABawkeag3uWRAdeFZ9uSB7vJoeTg"
    }
};